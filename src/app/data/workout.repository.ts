import { Injectable } from '@angular/core';
import { getRepository, Repository } from 'typeorm';
import { Workout } from './workout';
import { BaseRepository } from 'src/app/data/base.repository';

@Injectable({
    providedIn: 'root',
})
export class WorkoutRepository extends BaseRepository<Workout> {
    tableName = 'workout';
}
