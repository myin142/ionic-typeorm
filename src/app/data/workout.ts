import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('workout')
export class Workout {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @Column()
    public finishDate: Date;

}
