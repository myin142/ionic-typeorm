import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseService } from './data/database.service';
import { WorkoutRepository } from './data/workout.repository';
import { Workout } from './data/workout';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private database: DatabaseService,
        private workoutRepository: WorkoutRepository,
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        await this.platform.ready();
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        await this.database.initDatabase();

        const workout = new Workout();
        workout.name = 'Workout';
        workout.finishDate = new Date();

        await this.workoutRepository.repository.save(workout);
        console.log(await this.workoutRepository.repository.find());
    }
}
